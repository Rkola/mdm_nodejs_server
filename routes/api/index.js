const router = require('express').Router();
const print = console.log;

print("|    Loading all Contorllers...");

// load all controllers
var login = require("../../controller/login");
var getOrgAndInstallations = require("../../controller/getOrgAndInstallations");
var getLocEntityTypes = require("../../controller/getLocEntityTypes");
var getMetersByOrgId = require("../../controller/getMetersByOrgId");
var upsertOrgLogo =require("../../controller/upsertOrgLogo");
var addCustomer =require("../../controller/addCustomer");
var updateCustomer   =require("../../controller/updateCustomer");
var addLocEntityMeters = require("../../controller/addLocEntityMeters");
var addMeters = require("../../controller/addMeters");
var updateMeterStatus = require("../../controller/updateMeterStatus");
var updateOrgProfile = require("../../controller/updateOrgProfile");
var getMeterKinds = require("../../controller/getMeterKinds");
var getCustomers = require("../../controller/getCustomers");
var getOrgDetails = require("../../controller/getOrgDetails");
var getCustomerInfo = require("../../controller/getCustomerInfo");
var getCustomerInvoiceInfo = require("../../controller/getCustomerInvoiceInfo");
var getInvoiceInfo = require("../../controller/getInvoiceInfo");
var getTariffs = require("../../controller/getTariffs");
var getOrgCustomFields = require("../../controller/getOrgCustomFields");
var generateInvoice = require("../../controller/generateInvoice");
var getInvoices = require("../../controller/getInvoices");
var deleteCustomer = require("../../controller/deleteCustomer");
var deleteCustomField = require("../../controller/deleteCustomField");
var deleteTariff = require("../../controller/deleteTariff");
var getLossData = require("../../controller/getLossData");
var createLocEntityHeirarchy = require("../../controller/createLocEntityHeirarchy");
var updateLocEntityTree = require("../../controller/updateLocEntityTree");
var getLocEntityHeirarchy = require("../../controller/getLocEntityHeirarchy");
var upsertOrgCustomField = require("../../controller/upsertOrgCustomField");
var upsertOrgTariff = require("../../controller/upsertOrgTariff");
var getOrgTariffs = require("../../controller/getOrgTariffs");
var updateDefaultInvoiceTemplate = require("../../controller/updateDefaultInvoiceTemplate");







router.post('/login', login);

router.get('/getOrgDetails/:orgId', getOrgDetails);

router.get('/getMeterKinds', getMeterKinds);

router.get('/getLocEntityTypes', getLocEntityTypes);

router.post('/createLocEntityTree', createLocEntityHeirarchy);

router.post('/updateLocEntityTree', updateLocEntityTree);

router.post('/upsertOrgLogo', upsertOrgLogo); 

router.post('/upsertOrgCustomField', upsertOrgCustomField); 

router.post('/upsertOrgTariff', upsertOrgTariff); 

router.post('/addCustomer', addCustomer); 

router.post('/updateCustomer', updateCustomer); 

router.get('/getLocEntities/:orgId', getLocEntityHeirarchy);

router.get('/getMetersbyOrg/:orgId', getMetersByOrgId);

router.post('/addLocEntityMeters', addLocEntityMeters);

router.post('/addMeters', addMeters);

router.post('/updateMeterStatus', updateMeterStatus);

router.get('/getCustomers/:orgId', getCustomers);

router.get('/getCustomerInfo/:custId', getCustomerInfo);

router.delete('/customer/:custId', deleteCustomer);

router.delete('/customfield/:customFieldId', deleteCustomField);

router.delete('/tariff/:tariffId', deleteTariff);

router.get('/getCustomerInvoiceInfo/:custId', getCustomerInvoiceInfo);

router.get('/getOrgCustomFields/:orgId', getOrgCustomFields);

router.get('/getOrgTariffs/:orgId', getOrgTariffs);

router.post('/getLossData', getLossData);

router.get('/getInvoiceInfo/:invoiceId', getInvoiceInfo);

router.get('/getTariffs/:orgId', getTariffs);

router.get('/get/:orgId', getTariffs);

router.post('/generateInvoice', generateInvoice);

router.get('/getInvoices/:orgId', getInvoices);

router.post('/updateOrgProfile', updateOrgProfile);

router.post('/updateDefaultInvoiceTemplate', updateDefaultInvoiceTemplate);










module.exports = router;
