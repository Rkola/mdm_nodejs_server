module.exports = {
  "server": {
    "port": 3000,
    "prefix": "/"
  },
  "db":{
    "host": "localhost",
    "port": 3306,
    "database": "mdm_db",
    "user": "root",
    "password": "tkpadmin123",
    "dialect": "mysql"
  },
  "influx":{
    "host":"localhost",
    "db":"TekpeaDB"
  },
  "auth":{
    "expiryTime":86400
  },
  "cc":{
    "tekpea": {
      "providerId": "1",
      "providerKey": "38008dd81c2f4d7985ecf6e0ce8af1d1",
      "timeout": 10000
    }
  }
}