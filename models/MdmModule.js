module.exports = function(sequelize, Sequelize) {
  return sequelize.define('mdm_module', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    path: {
      type: Sequelize.CHAR(255),
      allowNull: false
    }
  }, {
    tableName: 'mdm_module'
  })
}

