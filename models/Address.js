module.exports = function(sequelize, Sequelize) {
  return sequelize.define('address', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    address: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    city: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    postal_code: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    country: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    state: {
      type: Sequelize.CHAR(255),
      allowNull: false
    }
  }, {
    tableName: 'address'
  })
}