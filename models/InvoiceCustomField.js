module.exports = function(sequelize, Sequelize) {
  return sequelize.define('invoice_custom_field', {
    invoice_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    },
    custom_field_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    },
    custom_field_value:{
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'invoice_custom_field'
  })
}

