module.exports = function(sequelize, Sequelize) {
  return sequelize.define('gateway_ref_meter', {
    gateway_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    },
    meter_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    },
    ct_ratio: {
      type: Sequelize.DOUBLE(),
      allowNull: true
    }
  }, {
    tableName: 'gateway_ref_meter'
  })
}

