module.exports = function(sequelize, Sequelize) {
  return sequelize.define('meter_kind', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    kind_name: {
      type: Sequelize.CHAR(255),
      allowNull: true,
      UNIQUE:true
    },
    family: {
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'meter_kind'
  })
}

