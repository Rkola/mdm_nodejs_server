module.exports = function(sequelize, Sequelize) {
  return sequelize.define('invoice_meter', {
    invoice_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
    },
    meter_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
    }
  }, {
    tableName: 'invoice_meter'
  })
}

