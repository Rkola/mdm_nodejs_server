module.exports = function(sequelize, Sequelize) {
  return sequelize.define('invoice', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    customer_idx:{
      type:Sequelize.BIGINT(20),
      allowNull: false
    },
    period_from:{
      type: Sequelize.DATE,
      allowNull: false
    },
    period_to:{
      type: Sequelize.DATE,
      allowNull: false
    },
    due_date:{
      type: Sequelize.DATE,
      allowNull: false
    },
    amount: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    bill_status: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    user_idx:{
      type: Sequelize.DOUBLE(),
      allowNull: false
    },
    tariff_idx:{
      type: Sequelize.DOUBLE(),
      allowNull: false
    },
    org_idx:{
      type: Sequelize.DOUBLE(),
      allowNull: false
    },
    hide_custom_field:{
      type:Sequelize.TINYINT(1)
    },
    invoice_no: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
  }, {
    tableName: 'invoice'
  })
}
