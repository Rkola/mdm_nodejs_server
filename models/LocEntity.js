

module.exports = function (sequelize, Sequelize) {

  const loc_entity =  sequelize.define('loc_entity',  {
    id: {
      type: Sequelize.CHAR(255),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.CHAR(255),
      allowNull: false
    },
    description: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    org_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    loc_entity_type_idx:{
      type:Sequelize.BIGINT(20),
      allowNull:false
    },
    latitude: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    longitude: {
      type: Sequelize.DOUBLE,
      allowNull: true
    }
  }, {
      tableName: 'loc_entity',
      hierarchy:true,
      // onDelete: 'CASCADE'
});
return loc_entity;
}
