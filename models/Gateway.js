module.exports = function(sequelize, Sequelize) {
  return sequelize.define('gateway', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    name: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    serial_number: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    type: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    data_server_id: {
      type: Sequelize.DOUBLE(),
      allowNull: true
    }
  }, {
    tableName: 'gateway'
  })
}