module.exports = function(sequelize, Sequelize) {
  return sequelize.define('user_role', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    active_mdm_module_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false
    }
  }, {
    tableName: 'user_role'
  })
}

