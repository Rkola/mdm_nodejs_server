module.exports = function(sequelize, Sequelize) {
  return sequelize.define('map_ref_meter', {
    ref_meter_idx: {
      type:  Sequelize.INTEGER(20).UNSIGNED,
    },
    meter_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    }
  }, {
    tableName: 'map_ref_meter'
  })
}

