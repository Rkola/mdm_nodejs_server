module.exports = function(sequelize, Sequelize) {
  return sequelize.define('customer', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    salutation: {
      type: Sequelize.CHAR(32),
      allowNull: true
    },
    first_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    last_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    org_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    customer_address_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    billing_address_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    active: {
      type: Sequelize.CHAR(10),
      allowNull: true,
      default:'false'
    },
    printable_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    phone: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    email: {
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'customer'
  })
}

