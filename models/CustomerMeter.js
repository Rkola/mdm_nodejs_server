module.exports = function(sequelize, Sequelize) {
  return sequelize.define('customer_meter', {
    customer_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
    },
    meter_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
    }
  }, {
    tableName: 'customer_meter'
  })
}

