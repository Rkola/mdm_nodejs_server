module.exports = function(sequelize, Sequelize) {
  return sequelize.define('tariff', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    currency: {
      type: Sequelize.CHAR(64),
      allowNull: true
    },
    default_cost: {
      type: Sequelize.DOUBLE,
      allowNull: false
    },
    min_monthly_charge: {
      type: Sequelize.DOUBLE,
      allowNull: false
    },
    name: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    description: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    org_idx: {
      type: Sequelize.DOUBLE,
      allowNull: false
    }
  }, {
    tableName: 'tariff'
  })
}

