const Sequelize = require('sequelize-hierarchy')();
const dbConfig = require("../config/config").db;
const linkModels = require("../utils/linkModels");
const fs = require('fs');
const print = console.log;

var models = {};
  
models.Sequelize = Sequelize;

models.db = new Sequelize({
  host: dbConfig.host,
  port: dbConfig.port,
  username: dbConfig.user,
  password: dbConfig.password,
  database: dbConfig.database,
  dialect: dbConfig.dialect,
  logging:false,
  define: {
    timestamps: false,
    underscored: false,
    freezeTableName: true,
    omitNull: true
  },
  operatorsAliases: {}
});

models.db
  .authenticate()
  .then(() => {
    console.log('Connection to the database has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// load all the models
print("|    Loading all Models...");
var modelFiles = fs.readdirSync(__dirname);

for (var i=0; i<modelFiles.length; i++) {
  var fileName = modelFiles[i].split(".")[0];
  
  if (fileName !== "index") {
    models[fileName] = models.db.import(fileName);
  }
}

// link all foreign keys & associations
linkModels(models);

module.exports = models;