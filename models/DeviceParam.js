module.exports = function(sequelize, Sequelize) {
  return sequelize.define('device_param', {
    id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    param_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    param_alias: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    param_type: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    unit: {
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'device_param'
  })
}