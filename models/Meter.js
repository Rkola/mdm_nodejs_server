module.exports = function(sequelize, Sequelize) {
  return sequelize.define('meter', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    serial_number: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    gateway_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    meter_kind_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    org_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    isOffline: {
      type: Sequelize.CHAR(30),
      allowNull: true
    },
    latitude: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    longitude: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    ct_ratio: {
      type: Sequelize.DOUBLE,
      allowNull: true
    }
  }, {
    tableName: 'meter'
  })
}

