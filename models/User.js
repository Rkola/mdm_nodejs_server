module.exports = function(sequelize, Sequelize) {
  return sequelize.define('user', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    email: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    password: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    user_role_idx: {
      type: Sequelize.DOUBLE(),
      allowNull: false
    },
    role: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    logo_file_name: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    organization_id: {
      type: Sequelize.DOUBLE(),
      allowNull: true
    }
  }, {
    tableName: 'user'
  })
}