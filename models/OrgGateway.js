module.exports = function(sequelize, Sequelize) {
  return sequelize.define('org_gateway', {
    org_idx: {
      type: Sequelize.BIGINT(20),
    },
    gateway_idx: {
      type: Sequelize.BIGINT(20),
    }
  }, {
    tableName: 'org_gateway'
  })
}

