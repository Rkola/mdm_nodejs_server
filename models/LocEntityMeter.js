module.exports = function(sequelize, Sequelize) {
  return sequelize.define('loc_entity_meter', {
    loc_entity_idx: {
      type: Sequelize.CHAR(255),
    },
    meter_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
    }
  }, {
    tableName: 'loc_entity_meter'
  })
}

