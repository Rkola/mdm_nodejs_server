module.exports = function(sequelize, Sequelize) {
  return sequelize.define('invoice_template', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    template_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    template_alias  : {
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'invoice_template'
  })
}