module.exports = function(sequelize, Sequelize) {
  return sequelize.define('loc_entity_type', {
    id: {
      type: Sequelize.BIGINT(20),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    loc_entity_type_name: {
      type: Sequelize.CHAR(64),
      allowNull: false
    }
  }, {
    tableName: 'loc_entity_type'
  })
}

