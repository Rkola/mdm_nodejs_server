module.exports = function(sequelize, Sequelize) {
  return sequelize.define('user_role_mdm_module', {
    user_role_idx: {
      type:  Sequelize.INTEGER(20).UNSIGNED,
    },
    mdm_module_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
    }
  }, {
    tableName: 'user_role_mdm_module'
  })
}

