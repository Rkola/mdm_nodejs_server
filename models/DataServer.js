module.exports = function(sequelize, Sequelize) {
  return sequelize.define('data_server', {
    id: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    protocol: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    url: {
      type: Sequelize.CHAR(32),
      allowNull: false
    },
    basepath: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    providerid: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    providerkey: {
      type: Sequelize.CHAR(64),
      allowNull: false
    },
    port: {
      type: Sequelize.DOUBLE(),
      allowNull: true
    }
  }, {
    tableName: 'data_server'
  })
}