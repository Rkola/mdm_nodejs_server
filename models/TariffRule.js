module.exports = function(sequelize, Sequelize) {
  return sequelize.define('tariff_rule', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tariff_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false
    },
    min_range: {
      type: Sequelize.DOUBLE,
      allowNull: false
    },
    max_range: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    cost_kwh: {
      type: Sequelize.DOUBLE,
      allowNull: true
    }
  }, {
    tableName: 'tariff_rule'
  })
}

