module.exports = function(sequelize, Sequelize) {
  return sequelize.define('organization', {
    id: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    description: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    address_idx: {
      type: Sequelize.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    logo_file_name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    tag_line: {
      type: Sequelize.CHAR(255),
      allowNull: true
    }
  }, {
    tableName: 'organization'
  })
}