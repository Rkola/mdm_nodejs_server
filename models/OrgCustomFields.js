module.exports = function(sequelize, Sequelize) {
  return sequelize.define('org_custom_fields', {
    id: {
      type: Sequelize.CHAR(255),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    default: {
      type: Sequelize.CHAR(255),
      allowNull: true
    },
    org_idx: {
      type: Sequelize.BIGINT(20),
      allowNull: true
    },
  }, {
    tableName: 'org_custom_fields'
  })
}

