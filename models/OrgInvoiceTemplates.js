module.exports = function(sequelize, Sequelize) {
  return sequelize.define('org_invoice_templates', {
    org_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false
    },
    invoice_template_idx: {
      type: Sequelize.INTEGER(20).UNSIGNED,
      allowNull: false
    },
    isDefault: {
      type: Sequelize.BOOLEAN,
      defaultValue: 0
    }
  }, {
    tableName: 'org_invoice_templates'
  })
}

