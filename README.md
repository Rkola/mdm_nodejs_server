# MDM Server #

### About ###

This repository is the backend logic for MDM application. It handles all the API calls for the MDM application. It is also used to fetch the data from Influx DB( a time series database where all the device samples are stored.) 

### Prerequisites ###

* MySQL Server
* Database named "mdm_db"
* User credentials with read/write access to "mdm_db" database
* InfluxDB
* Influx Database named "TekpeaDB"


### How do I get set up? ###

* ```git clone```
* cd into the folder
* Within the config folder, make a copy to config.js.bak into config.js
* Make changes to config.js as per the local environment
* ```npm install```

### Setup on staging-mum machine

We use **pm2** for managing this node process in production. Please start the pm2 process using 
```
pm2 start app
```

