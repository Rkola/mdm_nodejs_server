const config = require("./config/config");
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const influx = require('influx');
const print = console.log;

const influxHelper = require('./utils/influxDBHelper');


process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"


var app = express();

// configure logging
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({'type':'*/json'}));

print("***********************************");
print("*                                 *");
print("* Starting MDM Server *");
print("*                                 *");
print("***********************************");

 
// load models
var models = require("./models");

// load all Custom middleware
var passModels = require("./utils/passModels")(models); // make "models" available in response.locals
var dbErrorHandler = require("./utils/dbErrorHandler"); // send 500 response for db errors

app.use(passModels);
app.use(dbErrorHandler);

// load routes
var api = require("./routes/api");
var gateway = require("./controller/gateway");
var influxController = require("./controller/influxController");


var influxQuerybuilder = require("./utils/influxQueryBuilder");

app.use("/mdm/api", api);
app.use("/mdm/gateway", gateway);
app.use("/mdm/influx", influxController);
app.use('/mdm/public', express.static(path.join(__dirname, '/public')));
//connect to influxDb

var influxClient = influxHelper.influxClient();

influxClient.getDatabaseNames()
  .then(names => {
   console.log(names)
  })
  .catch(err => {
    console.error(`Error connecting to Influx database!`)
  })

// connect to database
models.db.sync().then(() => {
  print(`|    Server starting at ${new Date().toLocaleString()}`);
  app.listen(config.server.port, () => {
    print(`|    Server running on port ${config.server.port}`);
  });
}).catch(error => {
  print(`|    Error connecting to Database. Error= ${error}`);
})
