var influxHelper = require('../utils/influxDBHelper');
var influxQueryBuilder = require('../utils/influxQueryBuilder');
module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.body.orgId;
    var startDate = request.body.startDate;
    var endDate = request.body.endDate;
    startDate = new Date(startDate).getTime() * 1000000;
    endDate = new Date(endDate).getTime() * 1000000;
    var interpolate = function (t1, x1, t2, x2, t) {
        var interpolateValue = (x1 + ((x2 - x1) * (t - t1) / (t2 - t1)))
        return interpolateValue;
    }
    try {
        var refMeters = await models.Organization.findOne({
            where: {
                id: orgId
            },
            attributes: ['name'],
            include: [
                {
                    model: models.Gateway, as: "gateways", attributes: ['serial_number'],
                    include: [{
                        model: models.Meter, as: "refMeters",
                        attributes: ['serial_number','ct_ratio'],
                        include: [{ model: models.Meter, as: "mappedMeters", attributes: ['serial_number','ct_ratio'] },
                        ]
                    }]
                }
            ]
        });

        var influxClient = influxHelper.influxClient();
        var meterData = [];
        var refMeterData = [];
        for (const gateway of refMeters.gateways) {
            for (const refMeter of gateway.refMeters) {
                for (const meter of refMeter.mappedMeters) {
                    const getCount = influxQueryBuilder.getCount((meter.serial_number).toString(), 'activeenergy_total');
                    const checkMeterParam = await influxClient.query(getCount);
                    var paramTable = (checkMeterParam.length != 0 && checkMeterParam[0].hasOwnProperty('count_val')) ? 'activeenergy_total' : 'activeenergy_in_total';
                    const startDateInterpolationQuery1 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + meter.serial_number + "\' and \"time\" <= " + startDate + " order by time desc limit 1";

                    const startDateInterpolationQuery2 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + meter.serial_number + "\' and \"time\" >= " + startDate + " order by time asc limit 1";
                    const endDateInterpolationQuery1 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + meter.serial_number + "\' and \"time\" <=" + endDate + " order by time desc limit 1";
                    const endDateInterpolationQuery2 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + meter.serial_number + "\' and \"time\" >= " + endDate + " order by time asc limit 1";
                    await influxClient.query(startDateInterpolationQuery1).then(async data => {
                        var x1 = data[0].value;
                        var t1 = new Date(data[0].time).getTime() * 1000000;
                        await influxClient.query(startDateInterpolationQuery2).then(async data => {
                            var x2 = data.length != 0 ? data[0].value : "";
                            var t2 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                            await influxClient.query(endDateInterpolationQuery1).then(async data => {
                                var x3 = data.length != 0 ? data[0].value : "";
                                var t3 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                                await influxClient.query(endDateInterpolationQuery2).then(async data => {
                                    var x4 = data.length != 0 ? data[0].value : "";
                                    var t4 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                                    var interpolatedValue = (interpolate(t3, x3, t4, x4, endDate) - interpolate(t1, x1, t2, x2, startDate)).toFixed(2);
                                    meterData.push({
                                        'meterId': meter.serial_number,
                                        'consumption': meter.ct_ratio == null ? interpolatedValue : meter.ct_ratio * interpolatedValue
                                    });
                                });
                            });
                        });
                    });
                }
            }
        }
        for (const gateway of refMeters.gateways) {
            for (const refMeter of gateway.refMeters) {
                    const getCount = influxQueryBuilder.getCount((refMeter.serial_number).toString(), 'activeenergy_total');
                    const checkMeterParam = await influxClient.query(getCount);
                    var paramTable = (checkMeterParam.length != 0 && checkMeterParam[0].hasOwnProperty('count_val')) ? 'activeenergy_total' : 'activeenergy_in_total';
                    const startDateInterpolationQuery1 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + refMeter.serial_number + "\' and \"time\" <= " + startDate + " order by time desc limit 1";

                    const startDateInterpolationQuery2 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + refMeter.serial_number + "\' and \"time\" >= " + startDate + " order by time asc limit 1";
                    const endDateInterpolationQuery1 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + refMeter.serial_number + "\' and \"time\" <=" + endDate + " order by time desc limit 1";
                    const endDateInterpolationQuery2 = "select time,value from \"" + paramTable + "\" where \"meter_id\" = \'" + refMeter.serial_number + "\' and \"time\" >= " + endDate + " order by time asc limit 1";
                    await influxClient.query(startDateInterpolationQuery1).then(async data => {
                        var x1 = data[0].value;
                        var t1 = new Date(data[0].time).getTime() * 1000000;
                        await influxClient.query(startDateInterpolationQuery2).then(async data => {
                            var x2 = data.length != 0 ? data[0].value : "";
                            var t2 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                            await influxClient.query(endDateInterpolationQuery1).then(async data => {
                                var x3 = data.length != 0 ? data[0].value : "";
                                var t3 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                                await influxClient.query(endDateInterpolationQuery2).then(async data => {
                                    var x4 = data.length != 0 ? data[0].value : "";
                                    var t4 = data.length != 0 ? new Date(data[0].time).getTime() * 1000000 : "";
                                    var interpolatedValue = (interpolate(t3, x3, t4, x4, endDate) - interpolate(t1, x1, t2, x2, startDate)).toFixed(2);
                                    refMeterData.push({
                                        'refMeterId': refMeter.serial_number,
                                        'consumption': refMeter.ct_ratio == null ? interpolatedValue : refMeter.ct_ratio * interpolatedValue
                                    });
                                });
                            });
                        });
                    });
            }
        }
        response.send({meterData:meterData, refMeterData:refMeterData});
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}