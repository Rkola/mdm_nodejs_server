module.exports = async function (request, response) {
    var models = response.locals.models;
    var custId = request.params.custId;
    try {
        const customerInfo = await models.Customer.findOne({
            where: { id: custId },
            include: [{
                model: models.Meter,
                as: "meters",
                include: [{
                    model: models.MeterKind,
                    as: "meterkind"
                },
                {
                    model: models.Gateway,
                    as: "gateway"
                }]
            },
            {
                model: models.Invoice,
                as: "invoices"
            },{
                model:models.Address,
                as:'customer_address'
            },
            {
                model:models.Address,
                as:'billing_address'
            }]
        });
        response.send(customerInfo);
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}