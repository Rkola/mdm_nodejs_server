module.exports = async function (request, response) {
    var models = response.locals.models;
    var customFieldId = request.params.customFieldId;
    try {
    var deleteCustomFieldCount =   await models.OrgCustomFields.destroy({
            where: { id: customFieldId }
        })
        if(deleteCustomFieldCount>0 && deleteCustomFieldCount == 1){
            response.send({success:true})
        }
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}