var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
module.exports = async function (request, response) {
    var models = response.locals.models;
    var form = new formidable.IncomingForm();
    form.parse(request, function (err, fields, files) {
      var oldpath = files.logo.path;
      var newpath = "./public/"+files.logo.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        response.send({success:true})
      });
    });
}