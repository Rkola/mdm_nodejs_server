//
//  required libraries (third party vendors)
//

var express = require('express');
var path = require('path');
var influxHelper = require('../utils/influxDBHelper');
var influxQueryBuilder = require('../utils/influxQueryBuilder');
//
//  router
//

var router = express.Router();

var influxClient = influxHelper.influxClient();

router.get('/measurements', async function (req, res) {
  influxClient.getMeasurements().then(names => {
    res.send(names)
  });
});

router.post('/measurementdata', async function (req, res) {
  var query = influxQueryBuilder.generateMeanValuesQuery(req.body.meterId, req.body.measurement, req.body.startTime, req.body.endTime, req.body.timeRange);
  await influxClient.query(query).then(data => {
    res.send(data)
  });
});

router.post('/allmeasurementsdata', async function (req, res) {
  var dataArray = [];
  await influxClient.getMeasurements().then(async names => {
    for (const m of names) {
      var query = influxQueryBuilder.generateMeanValuesQuery(req.body.meterId, m, req.body.startTime, req.body.endTime, req.body.timeRange);
      var measurementUnit = await res.locals.models.DeviceParam.findOne({ where: { param_name: m } });
      await influxClient.query(query).then(data => {
        dataArray.push({ param: m, data: data, unit: (measurementUnit != null) ? measurementUnit.unit : "" })
      });
    }
    let vt_ratio_index = dataArray.findIndex(m => m.param == "vt_ratio");
    if (vt_ratio_index > -1) {
      dataArray.splice(vt_ratio_index, 1);
    }
    res.send(dataArray)
  }).catch(err => {
    console.log(err)
  })
}); ""

router.post('/getConsumption', async function (req, res) {
  var maxValuesQuery = influxQueryBuilder.generateMaxValuesQuery(req.body.meterId, req.body.measurement, req.body.startTime, req.body.endTime, req.body.timeRange);
  var minValuesQuery = influxQueryBuilder.generateMinValuesQuery(req.body.meterId, req.body.measurement, req.body.startTime, req.body.endTime, req.body.timeRange);
  influxClient.query(maxValuesQuery).then(data => {
    var maxValues = data;
    influxClient.query(minValuesQuery).then(data => {
      var minValues = data;
      var consumptionArray = [];
      maxValues.forEach(function (point, i) {
        if (point.time._nanoISO == minValues[i].time._nanoISO) {
          consumptionArray.push({ time: minValues[i].time._nanoISO, mean: Number(point.max) - (minValues[i].min) })
        }
      })
      var consumptionValue = maxValues[maxValues.length - 1].max - minValues[0].min
      res.send(consumptionArray);
    });
  });
});

router.post('/getEnergyConsumption', async function (req, res) {
  const getCount = influxQueryBuilder.getCount((req.body.meterId).toString(), 'activeenergy_total');
  const checkMeterParam = await influxClient.query(getCount);
  var meterParam = (checkMeterParam.length != 0 && checkMeterParam[0].hasOwnProperty('count_val')) ? 'activeenergy_total' : 'activeenergy_in_total';
  const getLastValueQuery = influxQueryBuilder.getLastValueQuery(req.body.meterId, meterParam, req.body.startTime, req.body.endTime);
  const getFirstValueQuery = influxQueryBuilder.getFirstValueQuery(req.body.meterId, meterParam, req.body.startTime, req.body.endTime);
  const lastValue = await influxClient.query(getLastValueQuery);
  const firstValue = await influxClient.query(getFirstValueQuery);
  if (lastValue[0] != undefined && firstValue[0] != undefined) {
    var consumptionValue = lastValue[0].last - firstValue[0].value;
  } else {
    var consumptionValue = 0
  }
  res.send({ 'energy': consumptionValue });
});

router.post('/writePoint', async function (req, res) {
  //point received ==> voltage_ph3_neu,meter_id=9005000000002038 value=239.6 1615475610000000000
  //parsing the point ==> {measurement:"voltage_ph3_neu", tags:{meter_id:"9005000000002038"},fields:{value:239.6},timestamp:1615475610000000000}
  if(req.body.point != null && req.body.point != ""){
    influxClient.writePoints([
      {
        measurement: req.body.point.split(",")[0],
        tags: { meter_id: req.body.point.split(",")[1].split("=")[1].split(" ")[0] },
        fields: { value: Number(req.body.point.split(",")[1].split("=")[2].split(" ")[0]) },
        timestamp: Number(req.body.point.split(",")[1].split("=")[2].split(" ")[1]),
      }
    ]).then((a) => {
      res.send({ success: true })
    }).catch(err => {
      console.error(`Error saving data to InfluxDB! ${err.stack}`)
    })
  }else if(req.body.point != null){
    res.send({ success: true })
  }
  

});
module.exports = router;
