module.exports = async function (request, response) {
    var models = response.locals.models;
    try {
        var locEntityTypes = await models.LocEntityType.findAll({
            attributes:["id","loc_entity_type_name"]
        }).then(function (types) {
        //    var leTypes = types.map((type) => {"id":type.id, "name":type.loc_entity_type_name});
           response.send(types);
        }).catch(function (error) {
            console.log(error)
        });
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}