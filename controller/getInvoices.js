module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
      await models.Invoice.findAll({
            where: { org_idx: orgId },
            include:[{
                model:models.Customer,
                as:"customer"
            }]
        }).then(function(invoices){
            response.send(invoices);
        });
        
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}