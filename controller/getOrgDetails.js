const Sequelize = require('sequelize');
module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        var orgDetails = await models.Organization.findOne({
            where: {
                id: orgId
            },
            include: [
                { model: models.Gateway, as:"gateways"},
                {model: models.Address, as:"address"},
                {model:models.InvoiceTemplate, as:"org_templates"}
            ]
        });
        // const loc_entity_heirarchy = await models.LocEntity.findAll({
        //     where: { "org_idx": orgDetails.id }, hierarchy: true,
        //     include: [
        //     //     {
        //     //     model: models.LocEntityType,
        //     //     as: "loc_entity_type",
        //     //     attributes: ["loc_entity_type_name"]
        //     // },
        //     {
        //         model: models.Meter,
        //         as: "meters"
        //     }]
        // });
        response.send({
            orgDetails: orgDetails
            // org_loc_entities: loc_entity_heirarchy
        });
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}
