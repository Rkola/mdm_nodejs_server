module.exports = async function (req, res) {
    var models = res.locals.models;
    var name = req.body.name;
    var description = req.body.description;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var tag = req.body.tag;
    var timezone = req.body.timezone;
    var type = req.body.type;
    var installation_id = req.body.installation_id;
    var address1 = req.body.address1;
    var address2 = req.body.address2;
    var city = req.body.city;
    var postal_code = req.body.postal_code;
    var state = req.body.state;
    var country = req.body.country;
    var meters = req.body.meters;

    try {
        const isInserted = await models.Installation.upsert({
            name: name,
            description: description,
            inst_group_id: instGrpId,
            time_zone: "IST",
            billno: "0"
        });
        const upsertedInstallation = await models.Installation.findOne({ where: { "name": name, "inst_group_id": instGrpId }, includes:["id"] });
        await  models.InstallationGateway.destroy({where:{installation_id:upsertedInstallation.id}});
        var inst_gateway = [];
        for(const gateway of gateways){
            const gatewayDetails = await models.Gateway.findOne({where:{serial_number : gateway},attributes:["id"]});
            inst_gateway.push({"gateway_id": gatewayDetails.id, "installation_id":upsertedInstallation.id, "currently_valid": 1});
        }
        // var inst_gateway = gateways.map((gw) => ({"gateway_id": gw, "installation_id":upsertedInstallation.id, "currently_valid": 1}));
        await  models.InstallationGateway.bulkCreate(inst_gateway);
        var msg = (isInserted == true) ? "Installation added successfully." : "Installation updated successfully."
        res.send({ status: true, message: msg });
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}