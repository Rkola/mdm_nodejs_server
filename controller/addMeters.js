
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.data;
    try {
        var result = await models.Meter
        .bulkCreate(postData, 
            {
                fields:["id", "name","serial_number","gateway_idx","meter_kind_idx", "org_idx","isOffline", "latitude","longitude"] ,
                updateOnDuplicate: ["isOffline", "latitude","longitude", "name"] 
            })
        .then(obj => {
            response.send({success:true})
        })
        .catch(error => {
            response.send(error);
        })
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}