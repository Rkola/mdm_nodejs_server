var influxHelper = require('../utils/influxDBHelper');
var influxQueryBuilder = require('../utils/influxQueryBuilder');

module.exports = async function (req, res) {
    var models = res.locals.models;
    var custId = req.body.custId;
    var start = req.body.startDate;
    var end = req.body.endDate;
    var due = req.body.dueDate;
    var tariffId = req.body.tariffId;
    var selectedMeters = req.body.selectedMeters;
    var customFields = req.body.customFields;
    var hideCustomfield = req.body.hide_custom_field;
    var influxClient = influxHelper.influxClient();
    try {
        var meterConsumtionDetails = [];
        var tariffDetails = await models.Tariff.findOne({
            where: {
                id: tariffId,
            }, include: [
                {
                    model: models.TariffRule, as: "rules"
                }],
                order:  [
                    [
                        {model: models.TariffRule, as: 'rules'},
                        'min_range',
                        'ASC'
                    ]
                ]
        });
        var orgId = await models.Customer.findOne({
            where: {
                id: custId,
            }, attributes: ['org_idx']
        });
        var calcAmount = function (consumption) {
            let amount = 0;
            if(tariffDetails.rules.length !=0){
               tariffDetails.rules.forEach(function(rule){
                    if(rule.max_range <= consumption && rule.max_range != null){
                       amount = amount + (rule.max_range - rule.min_range) * rule.cost_kwh;
                    }else if(rule.min_range <= consumption && rule.max_range == null){
                        amount = amount + (consumption - rule.min_range) * rule.cost_kwh;
                    }else if(rule.min_range < consumption && rule.max_range >= consumption && rule.max_range != null){
                        amount = amount + (consumption - rule.min_range) * rule.cost_kwh;
                    }
                });
                var maxMaxRangeValue = Math.max.apply(
                    Math,
                    tariffDetails.rules.map(function(rule) {
                      return rule.max_range;
                    })
                  );
                var hasOnlyMinValue = tariffDetails.rules.findIndex(rule => rule.max_range == null);
                if(maxMaxRangeValue <= consumption && hasOnlyMinValue == -1){
                    amount = amount + (consumption - maxMaxRangeValue) * tariffDetails.default_cost;
                }else if(maxMaxRangeValue <= consumption && hasOnlyMinValue != -1){
                    amount = amount + (consumption - maxMaxRangeValue) * tariffDetails.default_cost;
                }
            }else{
                amount = consumption * tariffDetails.default_cost;
            };
            return amount;
        }

        for (const meter of selectedMeters) {
            //interpolation needs to be implemented

            const getCount = influxQueryBuilder.getCount((meter.meterId).toString(), 'activeenergy_total');
            const checkMeterParam = await influxClient.query(getCount);
            var meterParam = (checkMeterParam.length != 0 && checkMeterParam[0].hasOwnProperty('count_val')) ? 'activeenergy_total' : 'activeenergy_in_total';
            const getLastValueQuery = influxQueryBuilder.getLastValueQuery(meter.meterId, meterParam, start, end);
            const getFirstValueQuery = influxQueryBuilder.getFirstValueQuery(meter.meterId, meterParam, start, end);
            const lastValue = await influxClient.query(getLastValueQuery);
            const firstValue = await influxClient.query(getFirstValueQuery);
            if(lastValue[0] != undefined && firstValue[0] != undefined){
                var consumptionValue = lastValue[0].last - firstValue[0].value;
            }else{
                var consumptionValue = 0
            }
            var meterConsumptionAmount = calcAmount(consumptionValue / 1000);

            meterConsumtionDetails.push({ meter: meter.meterId, consumption: consumptionValue/1000, amount: Number(meterConsumptionAmount).toFixed(2)});
        }
        var totalAmount = 0;
        if (selectedMeters.length != 1) {
            var initialValue = 0;
            totalAmount = meterConsumtionDetails.reduce((a, b) => Number(a) + Number(b.amount), initialValue);
            totalAmount = totalAmount.toFixed(2);
        }
        else {
            totalAmount =Number(meterConsumtionDetails[0].amount).toFixed(2);
        }
        var meterIDs = [];
        for (const meter of selectedMeters) {
            var meterDetails = await models.Meter.findOne({
                where: {
                    serial_number: meter.meterId,
                }, attributes: ['id']
            });
            meterIDs.push(meterDetails.id)
        }
        await models.Invoice.build({ customer_idx: custId, org_idx: orgId.org_idx, period_from: start, period_to: end, due_date: due, amount: totalAmount, bill_status: 'due', created_by: 2, tariff_idx: tariffId, invoice_no: custId + "-" + new Date().getTime(), user_idx: 9, hide_custom_field: hideCustomfield }).save()
            .then(async invoice => {
                for (const id of meterIDs) {
                    await models.InvoiceMeter.build({ meter_idx: id, invoice_idx: invoice.id }).save();
                }
                for (const field of customFields) {
                    await models.InvoiceCustomField.build({ custom_field_idx: field.custom_field_idx, invoice_idx: invoice.id, custom_field_value: field.custom_field_value }).save();
                }
                res.send({success:true, invoice_id:invoice.id})
            })
            .catch(error => {
                console.log(error)
                //need some error-handling
            });
        
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}
