
module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        const loc_entity_heirarchy = await models.LocEntity.findAll({
            where: { "org_idx": orgId }, hierarchy: true,
            order: [
                ['name', 'ASC'],
            ],
            include: [{
                model: models.LocEntityType,
                as: "loc_entity_type",
                attributes:["loc_entity_type_name"]
            },
            {
                model: models.Meter,
                as: "meters"
            }]
        });















        // await models.LocEntity.findAll({
        //     where: {  "id": 1 },
        //     include: [{
        //         model: models.LocEntity,
        //         as: 'descendents',
        //         hierarchy: true
        //       }],
        //       hierarchy: true
        //   });

        // await models.LocEntity.findOne({
        //     where: { name: 'Loc1.1.1' },
        //     include: [{ model: models.LocEntity, as: 'ancestors' }],
        //     order: [[{ model: models.LocEntity, as: 'ancestors' }, 'hierarchyLevel']]
        // });

        // await models.LocEntity.findAll({
        //     where: { name: "Loc1" },
        //     include: {
        //       model: models.LocEntity,
        //       as:"descendents",
        //       hierarchy: true
        //     }
        //   });
        //   console.log(loc_entity_heirarchy);
        response.send(loc_entity_heirarchy)
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}