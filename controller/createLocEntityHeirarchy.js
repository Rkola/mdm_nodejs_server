
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.data;
    // console.log(postData)
    try {
        var saveLocEntityTree = async function(tree){
            for (const entity of tree) {
                await buildAndSaveEntity(entity);
                if(entity.hasOwnProperty("children") && entity.children.length != 0){
                    await saveLocEntityTree(entity.children);
                }
            }
           
        }
        var buildAndSaveEntity = async function(entity){
           await models.LocEntity
            .build(entity)
            .save()
            .then(savedEntity => {
                console.log(savedEntity.name);
                console.log("******************SAVED Location Entity*******************");
            })
            .catch(error => {
                console.log("error");
                response.send(error);
            })
        }

        // saveLocEntityTree(postData);
        saveLocEntityTree(postData).then(() => {
            response.send({success:true})
        })
        .catch(error => {
            response.send(error);
        })
        // var x = await models.LocEntity.bulkCreate(locentityData);
       
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}