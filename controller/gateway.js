//
//  required libraries (third party vendors)
//

var express = require('express');
var path = require('path');
var gatewayHelper = require('../utils/gatewayHelper');
//
//  router
//

var router = express.Router();

//to fetch the devices list the status of the gateway.
router.get('/:gwId', async function (req, res) {
  var models = res.locals.models;
  var gwId = req.params.gwId;
  var gateway = await models.Gateway.findOne({
    where: { serial_number: gwId }
  });
  var dataServer = await models.DataServer.findOne({
    where: { id: gateway.data_server_id }

  });
  var dataServerObj = dataServer.dataValues;
  if (typeof dataServerObj == 'object') {
    try {
      const apiString = gatewayHelper.buildAPIString(dataServerObj, gwId, 'dev', '', '');
      var response = await gatewayHelper.sendGatewayRequest(apiString, 'GET', '', req.query);
      if (response.success) {
        res.send({ data: response.data.devices });
      } else {
        res.status(response.status).send({
          success: false,
          status: response.status
        })
      }
    } catch (error) {
      console.log(error)
    }
  }
})


//to fetch the datasources of the device
router.get('/:gwId/:deviceId/capability', async function (req, res) {
  var models = res.locals.models;
  var gwId = req.params.gwId;
  var gateway = await models.Gateway.findOne({
    where: { serial_number: gwId }
  });
  var dataServer = await models.DataServer.findOne({
    where: { id: gateway.data_server_id }

  });
  var dataServerObj = dataServer.dataValues;
  if (typeof dataServerObj == 'object') {
    try {
      const apiString = gatewayHelper.buildAPIString(dataServerObj, gwId, 'dev', req.params.deviceId, 'admin/capability');
      var response = await gatewayHelper.sendGatewayRequest(apiString, 'GET', '', req.query);
      if (response.success) {
        if (req.query.hasOwnProperty('ds')) {
          // var dsObj = response.data.ds.reduce(function(obj,item){
          //   obj[item.name] = item; 
          //   return obj;
          // }, {});
          res.send(response.data.ds);
        }

      } else {
        res.status(response.status).send({
          success: false,
          status: response.status
        })
      }
    } catch (error) {
      console.log(error)
    }
  }
});

router.post('/:gatewayId/:deviceId/realtime', async function (req, res) {
  console.log("")
  var models = res.locals.models;
  var gwId = req.params.gatewayId;
  var gateway = await models.Gateway.findOne({
    where: { serial_number: gwId }
  });
  var dataServer = await models.DataServer.findOne({
    where: { id: gateway.data_server_id }

  });
  var dataServerObj = dataServer.dataValues;
  if (typeof dataServerObj == 'object') {
    try {
      var postData = {
        cp: [{
          key: req.body.cp,
          value: req.body.value
        }]
      };

      const apiString = gatewayHelper.buildAPIString(dataServerObj, gwId, 'dev', req.params.deviceId, 'dscp');
      var response = await gatewayHelper.sendGatewayRequest(apiString, 'POST', postData, '');
      if (response.success) {
        res.send(response);
      } else {
        res.send({
          success: false,
          status: response.status
        })
      }
    } catch (error) {
      console.log(error)
    }
  }
});


module.exports = router;