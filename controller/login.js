module.exports = async function (req, res) {
    var models = res.locals.models;
    var username = req.body.username;
    var password = req.body.password;
    try {
        var userDetails = await models.User.findOne({
            where: {
                email: username
            },
            include: [
                {
                    model: models.UserRole,
                    as: "user_role",
                    include: [{
                        model: models.MdmModule,
                        as: 'modules',
                        attributes: ["description"],
                        through: {
                            attributes: []
                        }
                    },
                    {
                        model: models.MdmModule,
                        as: "active_module"
                    }]
                }

            ]
        });
        res.send(userDetails);
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}