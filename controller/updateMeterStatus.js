
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.meters;
    var isGatewayOffline = request.body.isGatewayOffline;
    var gateway =  request.body.gateway;
    try {
        if (isGatewayOffline == "true") {
            await models.Meter.update({isOffline: "true"},
                {where: {gateway_idx: gateway}})
                .then(obj => {
                    response.send({ success: true })
                })
                .catch(error => {
                    response.send(error);
                })
         } else {
            var result = await models.Meter
                .bulkCreate(postData,
                    {
                        fields: ["id", "name", "serial_number", "gateway_idx", "meter_kind_idx", "org_idx", "isOffline", "latitude", "longitude"],
                        updateOnDuplicate: ["isOffline", "latitude", "longitude", "name"]
                    })
                .then(obj => {
                    response.send({ success: true })
                })
                .catch(error => {
                    response.send(error);
                })
        }

    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}