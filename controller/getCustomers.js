module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        var customers = await models.Customer.findAll({
            where: {
                org_idx: orgId
            },
            include:[{
                model:models.Address,
                as:'customer_address'
            },
            {
                model:models.Address,
                as:'billing_address'
            }]
        });
        response.send(customers);
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}