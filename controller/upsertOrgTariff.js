module.exports = async function (req, res) {
    var models = res.locals.models;
    var postData = req.body.tariff;
    try {
        const isInserted = await models.Tariff.upsert(postData);
        const upsertedTariff = await models.Tariff.findOne({ where: { "name": postData.name}});
        await models.TariffRule.destroy({
            where :{
                tariff_idx:upsertedTariff.id
            }
        });
        let tariffRules = postData.rules.map(rule=>({ ...rule, tariff_idx:upsertedTariff.id}));
        await models.TariffRule.bulkCreate(tariffRules);
        res.send(isInserted);
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}