module.exports = async function (request, response) {
    var models = response.locals.models;
    var custId = request.params.custId;
    try {
        const invoices = await models.Invoice.findAll({
            where: { customer_idx: custId },
            attributes:["amount", "updated_at"]
        });
        // console.log(invoices);
        // invoices.
        var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var amount = [];
        // invoices.map()
        for(const i of invoices){
            let obj = amount.filter(x=>x.month ==months[new Date(i.updated_at).getMonth()]);
            if(obj.length > 0){
                obj.amount =obj.amount +Number(i.amount);
            }else{
                amount.push({amount:i.amount,month:months[new Date(i.updated_at).getMonth()]})
            }
        }
        for( const month of months){
            if(amount.filter(i=>i.month == month).length == 0){
                amount.push({amount:0,month:month})
            }
        }
        response.send(amount);
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}