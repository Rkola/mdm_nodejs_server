module.exports = async function (req, res) {
    var models = res.locals.models;
    var postData = req.body.custom_field;
    try {
        const isInserted = await models.OrgCustomFields.upsert(postData);
        res.send(isInserted);
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}