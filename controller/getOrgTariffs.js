module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        var tariffs = await models.Tariff.findAll({
            where: { org_idx: orgId },
            include: [
                {
                    model: models.TariffRule, as: "rules"
                }
            ]
        });
            response.send(tariffs);
        
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}