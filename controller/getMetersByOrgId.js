module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        var meters = await models.Meter.findAll({
            where: {
                org_idx: orgId
            },
            include:[{
                model:models.Gateway,
                as: "gateway"
            },
            {
                model:models.MeterKind,
                as: "meterkind"
            },
            {
                model:models.LocEntity,
                as:"locEntities"
            }
            ]
        });
        response.send(meters);

    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}