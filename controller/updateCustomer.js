module.exports = async function (req, res) {
    var models = res.locals.models;
    var customer = req.body.customer;
    var meters = req.body.meters;
    try {
        await models.Customer.upsert(customer);
        await models.Address.upsert(customer.billing_address);
        await models.Address.upsert(customer.customer_address);
        await models.CustomerMeter.destroy({
            where :{
                customer_idx:customer.id
            }
        });
        var customerMeters = meters.map(m => ({"customer_idx": customer.id, "meter_idx":m }));
            await models.CustomerMeter.bulkCreate(customerMeters);
            res.send({success:true});    
        
    } catch (e) {
        return false;
    }

}