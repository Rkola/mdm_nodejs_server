module.exports = async function (req, res) {
    var models = res.locals.models;
    var orgId = req.body.orgId;
    var defaultTemplateId = req.body.defaultTemplateId;
    try {
        await models.OrgInvoiceTemplates.update({isDefault:false},{returning: true, where: {org_idx:orgId} });
        await models.OrgInvoiceTemplates.update({isDefault:true},{returning: true, where: {org_idx:orgId, invoice_template_idx:defaultTemplateId} });
        res.send({success:true});    
    } catch (e) {
        return false;
    }

}