
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.data;
    try {
        var oldOrgDetails = await models.Organization.findOne({
            where: {
                id: postData.id
            }
        });
        var updateStatus = await models.Organization.update(
            {
                name:postData.name,
                logo_file_name: postData.logo_file_name,
                description: postData.description,
                tag_line: postData.tag_line
            },
            { where: { id: Number(postData.id) } }
        );
        var updateStatus = await models.Address.update(
            { "address": postData.address.address, "city": postData.address.city, "state": postData.address.state, "country": postData.address.country, "postal_code": postData.address.postal_code },
            { where: { id: Number(oldOrgDetails.address_idx) } }
        );
        var org = await models.Organization.findOne({
            where: {
                id: postData.id
            },
            include: [{
                model: models.Address,
                as: "address"
            },
            { model: models.Gateway, as:"gateways"}]
        });
        response.send({ org: org })
        // .success(result =>
        //  console.log(result)
        // )
        // .error(err =>
        //     console.log(err)
        // )
        // {"data":{"id":"8","name":"Tekpea","logo_file_name":"Screenshot from 2019-07-03 17-13-40.png","address":{"address":"org Address, street no, Building Name","city":"org City","state":"org State","country":"org Country","postal_code":"234611"}}}
        // await models.Organization.build({
        //     id: 8,
        //     name: "Tekpea123",
        //     logo_file_name: "logo_file_name",
        //     description: "desc",
        //     tag_line:"tag_line"
        // }).save();

        //    await models.Organization.findOne({ where: { id: postData.id } })

        //         .on('success', function (org) {
        //             // Check if record exists in db
        //             if (org) {
        //                 org.update({
        //                     name: postData.name,
        //                     logo_file_name: postData.logo_file_name,
        //                     description: postData.description
        //                 })
        //                     .success(function (data) { 
        //                         console.log(data)
        //                     })
        //             }
        //         })
        // console.log(postData);
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}