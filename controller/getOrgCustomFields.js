module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
      await models.OrgCustomFields.findAll({
            where: { org_idx: orgId },
        }).then(function(fields){
            response.send(fields);
        });
        
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}