module.exports = async function (request, response) {
    var models = response.locals.models;
    var custId = request.params.custId;
    try {
    var deleteCustomerCount =   await models.Customer.destroy({
            where: { id: custId }
        })
        if(deleteCustomerCount>0 && deleteCustomerCount == 1){
            response.send({success:true})
        }
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}