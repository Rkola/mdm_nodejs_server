const Sequelize = require('sequelize');
module.exports = async function (request, response) {
    var models = response.locals.models;
    var orgId = request.params.orgId;
    try {
        var orgDetails = await models.Organization.findOne({
            where: {
                id: orgId
            },
            include: [{
                model: models.InstallationGroup,
                attributes: ["id"]
            }]
        });

        var installationGrp = await models.InstallationGroup.findOne({
            where: {
                organization_id: orgId
            }
        });
        var installationGrpGateways = await models.InstallationGroupGateway.findAll({
            where: {
                installation_group_id: installationGrp.id,
            },
            attributes: [],
            include: [{
                model: models.Gateway,
                attributes: ['name', 'serial_number']
            }]
        });
        var installations = await models.Installation.findAll({
            where: {
                inst_group_id: installationGrp.id
            }
            // include: [
            //     //     {
            //     //     model: models.PhysicalEntity, as: "locations",
            //     //     required:false
            //     // },
            //     {
            //         model: models.Customeraccount, as: "customers",
            //         // include:[{
            //         //     model:models.Pe2meter, as : "meters"
            //         // }],
            //         required: false
            //     }]
        });
        for (const installation of installations) {
            var instPeMeter = [];
            await models.InstallationGateway.findAll({
                where: {
                    installation_id: installation.id,
                },
                attributes: [],
                include: [{
                    model: models.Gateway,
                    attributes: ['name', 'serial_number']
                }]
            }).then(function (gateways) {
                Object.assign(installation.dataValues, { gateways: gateways.map((gw) => (gw.gateway.serial_number)) })
            }).catch(function (error) {
                console.log(error)
            });
        }
        for (const installation of installations) {
            await models.PhysicalEntity.findAll({
                where: {
                    installation_id: installation.id,
                }
            }).then(async function (pes) {
                for (const pe of pes) {
                    await models.Pe2meter.findAll({
                        where: {
                            pe_id: pe.id,
                        },
                        attributes: [],
                        include: [{
                            model: models.Meter,
                            attributes: ['name', 'serial_number',]
                        }]
                    }).then(function (meters) {
                        Object.assign(pe.dataValues, { meters: meters.map((m) => (m.meter)) })
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
                // console.log(installation)
                // console
                // instPeMeter = [...instPeMeter, ...pes]
                Object.assign(installation.dataValues, { pes: pes })
                
            }).catch(function (error) {
                console.log(error)
            });
        }
        response.send({
            orgDetails: orgDetails,
            installationGrpGateways: installationGrpGateways,
            installations: installations
        });
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}