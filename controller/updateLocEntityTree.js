
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.data;
    try {
        for (const entity of postData) {
        await models.LocEntity.destroy({
            where:  { id:entity.id }
          });
        }
          var saveLocEntityTree = async function(tree){
            for (const entity of tree) {
                await buildAndSaveEntity(entity);
                if(entity.hasOwnProperty("meters") &&  entity.meters.length != 0){
                  var EntityMeters =  entity.meters.map((m) => ({"loc_entity_idx":entity.id, "meter_idx":m.id}));
                  await models.LocEntityMeter.bulkCreate(EntityMeters);
                }
                if(entity.hasOwnProperty("children") && entity.children.length != 0){
                    await saveLocEntityTree(entity.children);
                }
            }
           
        }
        var buildAndSaveEntity = async function(entity){
           await models.LocEntity
            .build(entity)
            .save()
            .then(savedEntity => {
                console.log(savedEntity.name);
                console.log("******************SAVED Location Entity*******************");
            })
            .catch(error => {
                console.log("error");
                response.send(error);
            })
        }

        // saveLocEntityTree(postData);
        saveLocEntityTree(postData).then(() => {
            response.send({success:true})
        })
        .catch(error => {
            response.send(error);
        })
       
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}