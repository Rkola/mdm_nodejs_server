const Sequelize = require('sequelize');
module.exports = async function (request, response) {
    var models = response.locals.models;
    var instIds = request.body.instIds;
    try {
        var instPeMeter = [];
        for (const inst of instIds) {
          var installation =  await models.Installation.findOne({
            where:{
                id:inst
            }
            });
            await models.PhysicalEntity.findAll({
                where: {
                    installation_id: inst,
                }
            }).then(async function (pes) {
                for (const pe of pes) {
                    await models.Pe2meter.findAll({
                        where: {
                            pe_id: pe.id,
                        },
                        attributes: [],
                        include: [{
                            model: models.Meter,
                            attributes: ['name', 'serial_number',]
                        }]
                    }).then(function (meters) {
                        Object.assign(pe.dataValues, { meters: meters.map((m) => (m.meter)) })
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
                // instPeMeter = [...instPeMeter, ...pes]
                instPeMeter.push({installation:installation, pes:pes})
            }).catch(function (error) {
                console.log(error)
            });
        }
        response.send(instPeMeter);
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}