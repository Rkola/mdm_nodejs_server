module.exports = async function (req, res) {
    var models = res.locals.models;
    var customer = req.body.customer;
    var meters = req.body.meters;
    try {
        var newCustomer =  await models.Customer.build(customer,{
            include: [
                {
                    model:models.Address,
                    as:'customer_address'
                },
                {
                    model:models.Address,
                    as:'billing_address'
    
            }]
        });
        newCustomer
        .save()
        .then(async function (){
            var customerMeters = meters.map(m => ({"customer_idx": newCustomer.id, "meter_idx":m }));
            await models.CustomerMeter.bulkCreate(customerMeters);
            res.send({success:true, custId:newCustomer.id});    
        })
        .catch(function (error) {
            res.send("Customer creation failed: " + error);
        });
    } catch (e) {
        return false;
    }

}