module.exports = async function (request, response) {
    var models = response.locals.models;
    var tariffId = request.params.tariffId;
    try {
    var deleteTariffCount =   await models.Tariff.destroy({
            where: { id: tariffId }
        })
        if(deleteTariffCount>0 && deleteTariffCount == 1){
            response.send({success:true})
        }
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}