module.exports = async function (req, res) {
    var models = res.locals.models;
    var name = req.body.name;
    var description = req.body.description;
    var gateways = req.body.gateways;
    var instGrpId = req.body.instGrpId;
    try {
        const isInserted = await models.Installation.upsert({
            name: name,
            description: description,
            inst_group_id: instGrpId,
            time_zone: "IST",
            billno: "0"
        });
        const upsertedInstallation = await models.Installation.findOne({ where: { "name": name, "inst_group_id": instGrpId }, includes:["id"] });
        await  models.InstallationGateway.destroy({where:{installation_id:upsertedInstallation.id}});
        var inst_gateway = [];
        for(const gateway of gateways){
            const gatewayDetails = await models.Gateway.findOne({where:{serial_number : gateway},attributes:["id"]});
            inst_gateway.push({"gateway_id": gatewayDetails.id, "installation_id":upsertedInstallation.id, "currently_valid": 1});
        }
        // var inst_gateway = gateways.map((gw) => ({"gateway_id": gw, "installation_id":upsertedInstallation.id, "currently_valid": 1}));
        await  models.InstallationGateway.bulkCreate(inst_gateway);
        var msg = (isInserted == true) ? "Installation added successfully." : "Installation updated successfully."
        res.send({ status: true, message: msg });
    } catch (error) {
        res.locals.dbErrorHandler(error);
    }
}