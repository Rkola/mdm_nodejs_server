module.exports = async function (request, response) {
    var models = response.locals.models;
    try {
        var meterKinds = await models.MeterKind.findAll();
        response.send(meterKinds);
        // await models.Organization.findAll({
        //     where: { id: orgId },
        //     include: [{
        //         model: models.Gateway,
        //         as: "gateways",
        //         include:[{
        //            model: models.Meter,
        //            as:"meters"
        //         }]
        //     }
        //     ]
        // }).then(function (result) {
        //     response.send(result[0].gateways)
        // });

    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}