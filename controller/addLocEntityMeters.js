
module.exports = async function (request, response) {
    var models = response.locals.models;
    var postData = request.body.data;
    try {
        var result = await models.LocEntityMeter
        .bulkCreate(postData)
        .then(obj => {
            response.send({success:true})
        })
        .catch(error => {
            response.send(error);
        })
    } catch (error) {
        response.locals.dbErrorHandler(error);
    }
}