const print = console.log;

module.exports = function (request, response, next) {
  response.locals.dbErrorHandler = function (error) {
    response.status(500).send({
      error   : true,
      message : "Database Error"
    });
    print(`DB Error = ${error.message}`);
  }

  next();
}