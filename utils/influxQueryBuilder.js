
const generateMaxValuesQuery = (meterId, measurement, startTime, endTime, timeRange) => {
  var query = `select MAX("value") from "${measurement}" where "meter_id" = '${meterId}' and "time" <= '${endTime}T23:59:00.000000000Z' and "time" >= '${startTime}' GROUP BY time(${timeRange}) fill(none)`;
  return query;
 };
 
 const generateMinValuesQuery = (meterId, measurement, startTime, endTime, timeRange) => {
   var query = `select MIN("value") from "${measurement}" where "meter_id" = '${meterId}' and "time" <= '${endTime}T23:59:00.000000000Z' and "time" >= '${startTime}' GROUP BY time(${timeRange}) fill(none)`;
   return query;
  };
 
 const generateMeanValuesQuery = (meterId, measurement, startTime, endTime, timeRange) => {
   var query = `select MEAN("value") from "${measurement}" where "meter_id" = '${meterId}' and "time" <= '${endTime}T23:59:00.000000000Z' and "time" >= '${startTime}' GROUP BY time(${timeRange})`;
   return query;
  };
 
 //  select last("value") from "activeenergy_total" where "meter_id" = '${meterId}' and "time" <= '${startDate.toISOString()}'
  const getLastValueQuery = (meterId, measurement, startTime, endTime) => {
   var query = `select LAST("value") from "${measurement}" where "meter_id" = '${meterId}' and "time" <= '${endTime}' and "time" >= '${startTime}'`;
   return query;
  };
 
  const getFirstValueQuery = (meterId, measurement, startTime, endTime) => {
   var query = `select "value" from "${measurement}" where "meter_id" = '${meterId}' and "time" <= '${endTime}' and "time" >= '${startTime}' order by time asc limit 1`;
   return query;
  };

  const writePointQuery = (writePoint) => {
    var query = `INSERT "${writePoint}"`;
    return query;
   };
 
  const getCount = (meterId, measurement, time) => {
   var query = `select count(*) from "${measurement}" where "meter_id" = '${meterId}'`;
   return query;
  };
 
 module.exports = {
   generateMaxValuesQuery: generateMaxValuesQuery,
   generateMinValuesQuery:generateMinValuesQuery,
   generateMeanValuesQuery: generateMeanValuesQuery,
   getFirstValueQuery:getFirstValueQuery,
   getLastValueQuery:getLastValueQuery,
   getCount:getCount,
   writePointQuery:writePointQuery
 }
 