
const Influx = require('influx');
const influxDBConfig = require("../config/config").influx;

//create and return influx client
const influxClient = () => {
 return new Influx.InfluxDB({
    host: influxDBConfig.host,
    database: influxDBConfig.db
  })
};

module.exports = {
  influxClient: influxClient
}