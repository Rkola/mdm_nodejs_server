module.exports = function (models) {
  return function (request, response, next) {
    response.locals.models = models;
    next();
  }
}