module.exports = function (models) {

    models.Organization.belongsToMany(models.Gateway, { as: 'gateways', through: models.OrgGateway, foreignKey: 'org_idx' })

    models.Organization.belongsTo(models.Address, { foreignKey: 'address_idx' , as:'address' });

    models.Gateway.belongsToMany( models.Organization, { as: 'org', through: models.OrgGateway, foreignKey: 'gateway_idx' })

    
    models.LocEntity.belongsTo(models.LocEntityType, {foreignKey:"id",  as:"loc_entity_type", onDelete: 'CASCADE',});
        
    

    models.LocEntity.belongsToMany(models.Meter, { as: 'meters', through: models.LocEntityMeter, foreignKey: 'loc_entity_idx', onDelete: 'CASCADE', })
    models.Meter.belongsToMany( models.LocEntity, { as: 'locEntities', through: models.LocEntityMeter, foreignKey: 'meter_idx', onDelete: 'CASCADE', })

    models.Meter.belongsTo( models.Gateway, { as: 'gateway', foreignKey: 'gateway_idx' });
    models.Meter.belongsTo( models.MeterKind, { as: 'meterkind', foreignKey: 'meter_kind_idx' });

    models.Gateway.hasMany(models.Meter, { foreignKey: 'gateway_idx', as: 'meters' });

    models.Organization.hasMany(models.Customer, { foreignKey: 'org_idx', as: 'customers'})
    models.Customer.belongsTo(models.Address, { foreignKey: 'customer_address_idx' , as:'customer_address'});
    models.Customer.belongsTo(models.Address, { foreignKey: 'billing_address_idx' , as:'billing_address' });
    
    models.Customer.belongsToMany(models.Meter, { as: 'meters', through: models.CustomerMeter, foreignKey: 'customer_idx' });
    models.Meter.belongsToMany( models.Customer, { as: 'customers', through: models.CustomerMeter, foreignKey: 'meter_idx' });
    models.Customer.hasMany(models.Invoice, { as: 'invoices', foreignKey: 'customer_idx' });

    models.Invoice.belongsTo(models.Customer, {foreignKey:"customer_idx",  as:"customer"});
    models.Invoice.belongsToMany(models.Meter, { as: 'meters', through: models.InvoiceMeter, foreignKey: 'invoice_idx' });
    models.Meter.belongsToMany( models.Invoice, { as: 'invoices', through: models.InvoiceMeter, foreignKey: 'meter_idx' });

    models.Invoice.belongsToMany(models.OrgCustomFields, { as: 'fields', through: models.InvoiceCustomField, foreignKey: 'invoice_idx' });
    models.OrgCustomFields.belongsToMany( models.Invoice, { as: 'invoices', through: models.InvoiceCustomField, foreignKey: 'custom_field_idx' });

    models.LocEntity.belongsTo( models.LocEntity,  { through: models.LocEntity, foreignKey: 'parentId', onDelete: 'CASCADE'});

    models.User.belongsTo(models.UserRole,{foreignKey:'user_role_idx', as:'user_role'});
    models.UserRole.belongsTo(models.MdmModule,{foreignKey:'active_mdm_module_idx', as:'active_module'});
    models.UserRole.belongsToMany(models.MdmModule, { as: 'modules', through: models.UserRoleMdmModule, foreignKey: 'user_role_idx' });
    models.MdmModule.belongsToMany( models.UserRole, { as: 'roles', through: models.UserRoleMdmModule, foreignKey: 'mdm_module_idx' });


    models.InvoiceTemplate.belongsToMany(models.Organization, { as: 'templates', through: models.OrgInvoiceTemplates, foreignKey: 'invoice_template_idx', onDelete: 'CASCADE', })
    models.Organization.belongsToMany( models.InvoiceTemplate, { as: 'org_templates', through: models.OrgInvoiceTemplates, foreignKey: 'org_idx', onDelete: 'CASCADE', })


    models.Gateway.belongsToMany(models.Meter, { as: 'refMeters', through: models.GatewayRefMeter, foreignKey: 'gateway_idx' });
    models.Meter.belongsToMany(models.Gateway, { through: models.GatewayRefMeter, foreignKey: 'meter_idx' });


    models.Meter.belongsToMany(models.Meter, { as: 'mappedMeters', through: models.MapRefMeter, foreignKey: 'ref_meter_idx' });
    models.Meter.belongsToMany( models.Meter, { as: 'metersMapped', through: models.MapRefMeter, foreignKey: 'meter_idx' });


    models.Tariff.hasMany(models.TariffRule, { foreignKey: 'tariff_idx', as: 'rules'})
    
    // models.Map.belongsTo( models.LocEntity,  { through: models.LocEntity, foreignKey: 'parentId', onDelete: 'CASCADE'});
    // models.InvoiceTemplate.belongsToMany(models.Organization, { as: 'org_invoice_templates', through: models.OrgInvoiceTemplate, foreignKey: 'invoice_template_idx', onDelete: 'CASCADE', })
    // models.Organization.hasMany(models.InvoiceTemplate, { foreignKey: 'org_idx',through: models.OrgInvoiceTemplate, as: 'org_templates' });
    // models.Organization.belongsTo( models.InvoiceTemplate, { as: 'org_templates', through: models.OrgInvoiceTemplate, foreignKey: 'org_idx', onDelete: 'CASCADE', })
}
