const axios = require('axios');
const https = require('https');
const http = require ('http');

//function to build the api string from the dataserver object of each device/gateway
const buildAPIString = (dataServerObj, gatewayId, idType, type, endpoint, queryParam) => {
  var APIString = '';
  var gatewayId = (typeof gatewayId === 'undefined' ||
    gatewayId === null ||
    gatewayId === '' ?
    '' :
    '/' + gatewayId);

  var idType = (typeof idType === 'undefined' ||
    idType === null ||
    idType === '' ?
    '' :
    '/' + idType);

  var type = (typeof type === 'undefined' ||
    type === null ||
    type === '' ?
    '' :
    '/' + type);

  var endpoint = (typeof endpoint === 'undefined' ||
    endpoint === null ||
    endpoint === '' ?
    '' :
    '/' + endpoint);

  var queryParam = (typeof queryParam === 'undefined' ||
    queryParam === null ||
    queryParam === '' ?
    '' :
    queryParam);

  if (dataServerObj != null) {
    APIString = dataServerObj.protocol + '://' + dataServerObj.url + ':' + dataServerObj.port + dataServerObj.basepath;
  }
  console.log(APIString + gatewayId + idType + type + endpoint + queryParam)
  return {
    url: APIString + gatewayId + idType + type + endpoint + queryParam,
    providerId: dataServerObj.providerid,
    providerKey: dataServerObj.providerkey,
    port:dataServerObj.port,
    apiURL:dataServerObj.url
  };
};


// { url: 'https://eu-svc.tekpea.com:8445/api/as/0000507691000a86',
//   providerId: '6',
//   providerKey: '38008dd81c2f4d7985ecf6e0ce8af1d1',
//   port: 8445,
//   apiURL: 'eu-svc.tekpea.com' }


const sendGatewayRequest = async (reqInfo, reqType, reqObj, queryParams) => {
  var gatewayResponse;
  var headers = {
    "Access-Control-Allow-Headers" : "*",
    'X-Tekpea-provider-id': reqInfo.providerId,
    'X-Tekpea-provider-key': reqInfo.providerKey,
    'Accept':'application/json'
  };
  var agentOptions = {
        host:reqInfo.apiURL,
        port:reqInfo.port,
        path:'/',
        rejectUnauthorized: false
  }
  var agent;
  if (reqInfo.protocol === 'https') {
     agent = new https.Agent(agentOptions);
  } else {
    agent = new http.Agent(agentOptions);
  }
  switch (reqType) {
    case 'GET':
      try {
        var apiGetResponse = await apiGetRequest(reqInfo, headers, agent, queryParams);
        if (apiGetResponse.data != null) {
          return {
            success: true,
            status: apiGetResponse.status,
            data: apiGetResponse.data
          }
        } else {
          return {
            success: false,
            status: apiGetResponse.status
          }
        }
      } catch (error) {
        console.log(error);
      }
      break;
    case 'POST':
      try {
        var apiPostResponse = await apiPostRequest(reqInfo, headers, agent, reqObj, queryParams);
        if (apiPostResponse.data != null) {
          return {
            success: true,
            status: apiPostResponse.status,
            data: apiPostResponse.data
          }
        } else {
          return {
            success: false,
            status: apiPostResponse.status
          }
        }
      } catch (error) {
        console.log(error);
      }
      break;
    case 'PUT':
    try {
      var apiPutResponse = await apiPutRequest(reqInfo, headers, agent, reqObj, queryParams);
      if (apiPutResponse.data != null) {
        return {
          success: true,
          status: apiPutResponse.status,
          data: apiPutResponse.data
        }
      } else {
        return {
          success: false,
          status: apiPutResponse.status
        }
      }
    } catch (error) {
      console.log(error);
    }
      break;
    case 'DELETE':
      try {
        var apiDeleteResponse = await apiDeleteRequest(reqInfo, headers, agent, reqObj, queryParams);
        if (apiDeleteResponse.data != null) {
          return {
            success: true,
            status: apiDeleteResponse.status,
            data: apiDeleteResponse.data
          }
        } else {
          return {
            success: false,
            status: apiDeleteResponse.status
          }
        }
      } catch (error) {
        console.log(error);
      }
      break;
  }
};

const apiGetRequest = async (reqInfo, headers, agent, queryParams) => {
  try {
    return await axios.get(reqInfo.url, {
      headers: headers,
      params: queryParams
    }, {
      httpsAgent: agent
    },{
      withCredentials: true
    });
  } catch (error) {
    return {
      data: null,
      status: error.response.status
    }
  }
};

const apiPostRequest = async (reqInfo, headers, agent, reqObj, queryParams) => {
  try {
    return await axios.post(reqInfo.url, reqObj, {
      headers: headers,
      params: queryParams,
      timeout: 1000
    }, {
      httpsAgent: agent
    });
  } catch (error) {
    return {
      data: null,
      status: error.response.status
    }
  }
};

const apiDeleteRequest = async (reqInfo, headers, agent, reqObj, queryParams) => {
  try {
    return await axios.delete(reqInfo.url, {
      headers: headers,
      params: queryParams,
      timeout: 1000
    }, {
      httpsAgent: agent
    });
  } catch (error) {
    return {
      data: null,
      status: error.response.status
    }
  }
};

const apiPutRequest = async (reqInfo, headers, agent, reqObj, queryParams) => {
  try {
    return await axios.put(reqInfo.url, reqObj, {
      headers: headers,
      params: queryParams,
      timeout: 1000
    }, {
      httpsAgent: agent
    });
  } catch (error) {
    return {
      data: null,
      status: error.response.status
    }
  }
};

module.exports = {
  buildAPIString: buildAPIString,
  sendGatewayRequest: sendGatewayRequest
}